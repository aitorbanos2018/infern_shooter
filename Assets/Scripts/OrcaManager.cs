﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcaManager : MonoBehaviour
{   
    [SerializeField] GameObject[] Formacion;

    public float timeLaunchEnemy;

    private float currentTime = 0;

    private int FormacionActual;

    // Update is called once per frame
    void Update()
    {
        FormacionActual = Random.Range(0,Formacion.Length);

        currentTime += Time.deltaTime;
        if(currentTime>timeLaunchEnemy){
            currentTime = 0;
            Instantiate(Formacion[FormacionActual],new Vector3(12,-5,0),Quaternion.identity,this.transform);
        }
    }
}
