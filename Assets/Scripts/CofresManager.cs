﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CofresManager : MonoBehaviour
{   
    [SerializeField] GameObject[] Formacion;

    public float timeLaunchCofres;

    private float currentTime = 0;

    private int FormacionActual;

    // Update is called once per frame
    void Update()
    {
        FormacionActual = Random.Range(0,Formacion.Length);

        currentTime += Time.deltaTime;
        if(currentTime>timeLaunchCofres){
            currentTime = 0;
            Instantiate(Formacion[FormacionActual],new Vector3(Random.Range(-7,7),8,0),Quaternion.identity,this.transform);
        }
    }
}
