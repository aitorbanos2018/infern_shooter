﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cienpies : MonoBehaviour
{
    // Start is called before the first frame update

    public Vector2 speed;
    public Vector2 limits;
    
    Transform graphics;
    void Awake()
    {

        speed.x = Random.Range(-5,-5);
        speed.y = Random.Range(5,5); 
    }

   
    void Update()
    {
        transform.Translate(speed*Time.deltaTime);

        if (transform.position.y > limits.y) {
            speed.y *= -1;
        }else if (transform.position.y < -limits.y) {
            speed.y *= -1;
        }
    }
    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Muro"){
            Destroy(this.gameObject);
        }
        
        if( other.tag == "PlayerAttack" ) {
           StartCoroutine(DestroyDemon());
        }
        
    }
IEnumerator DestroyDemon(){

        //Desactivo el grafico
        gameObject.SetActive(false);

        
        //Elimina el BoxCollider
        Destroy(gameObject.GetComponent<BoxCollider2D>());
        

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }


    
}
