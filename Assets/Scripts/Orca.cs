﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orca : MonoBehaviour
{
	// Start is called before the first frame update

	Vector2 speed;

	Transform graphics;
	//Time
	public float time;
	private float currentime;

	public Orcaweapon orka;
	void Awake()
	{
		currentime = 0;
		speed.x = Random.Range(-3, -1);
	}



	void Update()
	{
		currentime += Time.deltaTime;
		transform.Translate(speed * Time.deltaTime);
		if(currentime>=time){
			orka.shoot();
			currentime = 0;
		}
		

	}
	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Finish")
		{
			Destroy(this.gameObject);
		}

		if (other.tag == "PlayerAttack")
		{
			StartCoroutine(DestroyMeteor());
		}

	}

	IEnumerator DestroyMeteor()
	{

		gameObject.SetActive(false);
		Destroy(GetComponent<BoxCollider2D>());
		yield return new WaitForSeconds(1.0f);
		Destroy(this.gameObject);
	}
}








