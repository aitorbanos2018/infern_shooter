﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash : MonoBehaviour
{
    public float speed;

    public float PeriodOfTime;

    private float currentTimeTime = 0;


    // Update is called once per frame
    void Update()
    {
        currentTimeTime += Time.deltaTime;

        if (currentTimeTime < PeriodOfTime)
        {
            transform.Translate(speed * Time.deltaTime, 0, 0);

        }

        else
        {
            Destroy(gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "muro"|| other.tag == "Enemigo") {
        {
            Destroy(gameObject);
        }
    }
    }

}

