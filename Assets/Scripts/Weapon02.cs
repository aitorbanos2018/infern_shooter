﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon02 : MonoBehaviour
{

    public GameObject laserBullet;

    public float cadencia;

    public float GetCadencia()
    {
        return cadencia;
    }

    public void Shoot02()
    {
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
     
    }
}
