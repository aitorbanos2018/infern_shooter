﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private Vector2 axis;

    public Player nave;
    public Background bgback;
    public Background bgfront;

  

    // Update is called once per frame
    void Update () {
        axis.x = Input.GetAxis ("Horizontal");
        axis.y = Input.GetAxis ("Vertical");

        //Disparo
 
        if(Input.GetButton("Slash")){
            nave.Shoot();
        }

        if(Input.GetButton("dash")){
            nave.Shoot01();
        }


        nave.SetAxis(axis);
        bgback.SetVelocity(axis.x);
        bgfront.SetVelocity(axis.x);
    }

}
