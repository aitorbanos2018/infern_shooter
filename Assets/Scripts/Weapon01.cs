﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon01 : MonoBehaviour
{

    public GameObject laserBullet;

    public float cadencia;

    public float GetCadencia()
    {
        return cadencia;
    }

    public void Shoot01()
    {
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
     
    }
}
