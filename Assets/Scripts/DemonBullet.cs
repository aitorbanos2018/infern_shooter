﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonBullet : MonoBehaviour
{
    // Start is called before the first frame update

    Vector2 speed;


    Transform graphics;

    void Awake()
    {

        speed.x = Random.Range(-7,-5);
    }

   
    void Update()
    {
        transform.Translate(speed*Time.deltaTime);
        
    }
    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Muro"){
            Destroy(this.gameObject);
        }
        
        if( other.tag == "Player" ) {
            Destroy(this.gameObject);
        }
        
    }

}

