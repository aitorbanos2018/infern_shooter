﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Player : MonoBehaviour
{
 
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
 
    public float shootTime=0;
 
    public Weapon slash;

    public Weapon01 dash;

    public Weapon02 flechas;

    public Weapon03 fireball;
 
   
    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
 
    public int lives = 3;

    public int flechascont = 0;
    
    public int dashcont = 0;

    public int fireballcont = 0;
    public bool iamDead = false;
   
    // Update is called once per frame
    void Update () {
        if(iamDead){
            return;
        }
 
        shootTime += Time.deltaTime;
 
        transform.Translate (axis * speed * Time.deltaTime);
 
        if (transform.position.x > limits.x) {
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        }else if (transform.position.x < -limits.x) {
            transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }
 
        if (transform.position.y > limits.y) {
            transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
        }else if (transform.position.y < -limits.y) {
            transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
        }
    }
 
    public void ActualizaDatosInput(Vector2 currentAxis){
        axis = currentAxis;
    }
 
    public void SetAxis(float x, float y){
        axis = new Vector2(x,y);
    }
 
    public void SetAxis(Vector2 currentAxis){
        axis = currentAxis;
    }
 
    public void Shoot(){
        if(shootTime>slash.GetCadencia()){
            shootTime = 0f;
            slash.Shoot();
        }
    }

    public void Shoot01(){
        if(shootTime>dash.GetCadencia()){
            shootTime = 0f;
            dash.Shoot01();
            dashcont--;
        }
    }
    public void Shoot02(){
        if(shootTime>flechas.GetCadencia()){
            shootTime = 0f;
            flechas.Shoot02();
            flechascont--;
        }
    }

    public void Shoot03(){
        if(shootTime>flechas.GetCadencia()){
            shootTime = 0f;
            fireball.Shoot03();
            fireballcont--;
        }
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Enemigo") {
            StartCoroutine(DestroyShip());
        }
        if(other.tag == "Lava") {
            StartCoroutine(DestroyShip());
        }
        if(other.tag == "Vida") {
           lives=lives+1;
        }
        if(other.tag == "flecha") {
           flechascont=flechascont+20;
        }
        if(other.tag == "dash") {
           dashcont=dashcont+5;
        }
        if(other.tag == "fireball") {
           fireballcont=fireballcont+1;
        }
    }
 
    IEnumerator DestroyShip(){
        //Indico que estoy muerto
        iamDead=true;
 
        //Me quito una vida
        lives--;
 
        //Desactivo el grafico
        graphics.SetActive(false);
 
        //Elimino el BoxCollider2D
        collider.enabled = false;
 
        //Lanzo la partícula
 
        //Lanzo sonido de explosion
 
        //Desactivo el propeller
 
        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
       
        //Miro si tengo mas vidas
        if(lives>0){
            //Vuelvo a activar el jugador
            iamDead = false;
            graphics.SetActive(true);
            //Activo el propeller

            yield return new WaitForSeconds(0.2f);
            
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            graphics.SetActive(true);
            collider.enabled = true;
        }
    }
 
}
