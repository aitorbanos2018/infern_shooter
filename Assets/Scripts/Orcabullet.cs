﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orcabullet : MonoBehaviour
{
	// Start is called before the first frame update

	Vector2 speed;
	float arriba=0;
	private float abajo = 70;


	Transform graphics;

	void Awake()
	{


		speed.y = Random.Range(5, 5);
	}


	void Update()
	{
		transform.Translate(speed * Time.deltaTime);

		arriba++;
		if(arriba>= abajo){
			speed.y = 0;
		}
	}
	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Finish")
		{
			Destroy(this.gameObject);
		}

		if (other.tag == "Player")
		{
			StartCoroutine(DestroyMeteor());
		}

	}

	IEnumerator DestroyMeteor()
	{

		gameObject.SetActive(false);
		Destroy(GetComponent<BoxCollider2D>());
		yield return new WaitForSeconds(1.0f);
		Destroy(this.gameObject);
	}





}
