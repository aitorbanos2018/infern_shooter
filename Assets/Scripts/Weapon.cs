﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public GameObject laserBullet;

    public float cadencia;

    public float GetCadencia()
    {
        return cadencia;
    }

    public void Shoot()
    {
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
     
    }
}
