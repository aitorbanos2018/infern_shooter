﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire_Pixie_01 : MonoBehaviour
{
    Vector2 speed;

    // Start is called before the first frame update
    void Awake()
    {
       
        speed.x = Random.Range(-5,-2);
        speed.y = Random.Range(-2,2);
        
    }

    void Update(){
        transform.Translate(speed*Time.deltaTime);
               
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Muro") {
            Destroy(this.gameObject);
            }            
        else if (other.tag == "PlayerAttack"){    
            StartCoroutine(DestroyPixie());
        }
    }

    IEnumerator DestroyPixie(){

        //Desactivo el grafico
        gameObject.SetActive(false);

        //Elimina el BoxCollider
        Destroy(GetComponent<BoxCollider2D>());

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }


}