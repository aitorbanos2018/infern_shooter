﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muerte : MonoBehaviour
{
    // Start is called before the first frame update
        [SerializeField] GameObject graphics;


    void Awake()
    {

    }

   
    void Update()
    {

    }
    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy(this.gameObject);
        }
        
        if( other.tag == "Lava" ) {
            StartCoroutine(DestroyMeteor());
        }
        
    }

IEnumerator DestroyMeteor(){

    graphics.gameObject.SetActive(false);
    Destroy(GetComponent<BoxCollider2D>());
    yield return new WaitForSeconds(1.0f);
    Destroy(this.gameObject);
}  
}
