﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Demon : MonoBehaviour
{
 
    Vector2 speed;
    public Vector2 limits;

    private float avanzar= 0;
    private float parar= 45;
    private bool sentido = false;

    private float cargando= 0;
    private float disparo = 120;

    public GameObject meteorToInstantiate;
 
    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;

    	void Awake()
	{
		speed.x = -3;
        speed.y = 0;
	}
   
    // Update is called once per frame
    void Update () {

 		transform.Translate(speed * Time.deltaTime);

         avanzar++;
         cargando++;

         if(avanzar>parar && sentido == false){
             speed.x = 0;
             speed.y = 4;
         }
 
        if (transform.position.y > limits.y) {
            speed.y *= -1;
            sentido = true;
        }else if (transform.position.y < -limits.y) {
            sentido = false;
        }

        if(cargando>disparo){

            Instantiate(meteorToInstantiate,transform.position, Quaternion.identity, null);
            cargando = 0;

        }
 
    }
 
 
    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>

    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "PlayerAttack") {
            StartCoroutine(DestroyMeteor());
        }
        if(other.tag == "Finish") {
            Destroy(this.gameObject);
        }
    }

    	IEnumerator DestroyMeteor()
	{

		gameObject.SetActive(false);
		Destroy(GetComponent<BoxCollider2D>());
		yield return new WaitForSeconds(1.0f);
		Destroy(this.gameObject);
	}
 
}