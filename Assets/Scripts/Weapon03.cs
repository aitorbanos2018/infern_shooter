﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon03 : MonoBehaviour
{

    public GameObject laserBullet;

    public float cadencia;

    public float GetCadencia()
    {
        return cadencia;
    }

    public void Shoot03()
    {
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
     
    }
}
