﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    private Material mat;
    public float smooth = 1.0f;
    private float offsetX = 0.0f;

    private Vector2 textoffset;

    private float axisx;


    // Use this for initialization
    void Awake () {
        mat = GetComponent<Renderer> ().material;
        textoffset = new Vector2 (offsetX,0);
    }
    
    // Update is called once per frame, para que el fondo de mueva 
    void Update () {
        offsetX += (Time.deltaTime * (smooth  + 75*axisx/50));
        if(offsetX >= 100) offsetX -= 100;

        textoffset.x = offsetX;
        mat.SetTextureOffset("_MainTex", textoffset);
    }
    // Que la seua velocitat vaigue a partir de ña tecla que cliquem
    public void SetVelocity(float velx){
        axisx = velx;
    }
}
